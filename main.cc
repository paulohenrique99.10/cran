//-----------------------------------------------------Include
#include <fstream>
#include <string.h>
#include "ns3/csma-helper.h"
//#include "ns3/evalvid-client-server-helper.h"
#include "ns3/lte-helper.h"
#include "ns3/epc-helper.h"
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-module.h"
#include "ns3/lte-module.h"
#include "ns3/applications-module.h"
#include "ns3/point-to-point-helper.h"
#include "ns3/config-store.h"
#include <ns3/ofswitch13-module.h>
//#include "ns3/netanim-module.h"
#include "my-controller.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/flow-monitor.h"
#include "ns3/flow-monitor-helper.h"
#include "ns3/gnuplot.h"
#include "ns3/string.h"
#include "ns3/double.h"
#include <ns3/boolean.h>
#include <ns3/enum.h>
#include <iomanip>
#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
//#include "ns3/evalvid-client-server-helper.h"
//#include <ns3/my-epc-helper.h>
using namespace ns3;

static std::map<uint64_t, Ipv4Address> mymap;
static std::map<uint64_t, uint64_t> mymap2;
double rrhVazao[19] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
//-----------------------------------------------------Funções

	// retorna o mapa relacionando imsi x ip do epc
void
CallMap (Ptr<PointToPointEpcHelper> epcHelper)
{
  mymap = epcHelper->RecvMymap ();
}

//funcao que envia mapas e ordem de alocação ao controlador
void
SendToController (Ptr<MyController> MyCtrl, double rrhVazao[19])
{
  MyCtrl->Allocation (mymap, mymap2, rrhVazao);
}
// relaciona o imsi dos usuarios ao id da celula à qual estão conectados atualmente - é reececutado em handover por meio de callback
void
NotifyConnectionEstablishedUe (std::string context, uint64_t imsi, uint16_t cellid, uint16_t rnti)
{
  mymap2[imsi] = cellid;
}
//funcao que printa as metricas de qos para futura extracao -- pode ser otimizado por meio de processamento dentro da main
void
QoSMetricas (FlowMonitorHelper *fmhelper, Ptr<FlowMonitor> monitor, uint16_t numberOfNodes,
             double interval, Ptr<MyController> MyCtrl)
{
  static bool firstRun = true;
  static std::map<Ipv4Address, double> lastThroughput;
  double tempThroughput = 0.0;

  monitor->CheckForLostPackets ();
  std::map<FlowId, FlowMonitor::FlowStats> flowStats = monitor->GetFlowStats ();
  Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (fmhelper->GetClassifier ());
  //Obter o throughput individual -- associar os clientes da mesma rrh -- somar os throughputs e printar (por ora)
  for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator stats = flowStats.begin ();
       stats != flowStats.end (); ++stats)
    {
      // A tuple: Source-ip, destination-ip, protocol, source-port, destination-port
      Ipv4FlowClassifier::FiveTuple fiveTuple = classifier->FindFlow (stats->first);
      for (std::map<uint64_t, Ipv4Address>::iterator it = mymap.begin (); it != mymap.end (); it++)
        {
          if (ns3::Ipv4Address (fiveTuple.sourceAddress) == it->second)
            {
              if (firstRun)
                {
                  tempThroughput = (stats->second.txBytes * 8.0 /
                                    (stats->second.timeLastTxPacket.GetSeconds () -
                                     stats->second.timeFirstTxPacket.GetSeconds ()) /
                                    1024);
                  if ((tempThroughput > 99999 || tempThroughput < 0) || stats->second.txBytes == 0)
                    {
                      tempThroughput = 0;
                    }
                  lastThroughput.insert ({it->second, stats->second.txBytes});
                  rrhVazao[mymap2[it->first] - 1] += tempThroughput;
                  std::cout << "Flow ID: " << stats->first << " ; " << fiveTuple.sourceAddress
                            << " -----> " << fiveTuple.destinationAddress << ", adding "
                            << tempThroughput << " Kbps to rrh: " << mymap2[it->first] << std::endl;
                }
              else
                {
                  if (stats->second.txBytes == lastThroughput[it->second] ||
                      (stats->second.txBytes == 0))
                    {
                      tempThroughput = 0;
                    }
                  else
                    {
                      tempThroughput =
                          ((stats->second.txBytes - lastThroughput[it->second]) * 8.0 /
                           (stats->second.timeLastTxPacket.GetSeconds () -
                            (stats->second.timeLastTxPacket.GetSeconds () - interval)) /
                           1024);
                    }
                  if ((tempThroughput > 99999 || tempThroughput < 0) &&
                      (tempThroughput == tempThroughput))
                    {
                      tempThroughput = 0;
                    }
                  rrhVazao[mymap2[it->first] - 1] += tempThroughput;
                  std::cout << "Flow ID: " << stats->first << " ; " << fiveTuple.sourceAddress
                            << " -----> " << fiveTuple.destinationAddress << ", adding "
                            << tempThroughput << " Kbps to rrh: " << mymap2[it->first] << std::endl;
                  //std::cout << "Last txBytes: " << lastThroughput[it->second] << std::endl;
                  lastThroughput[it->second] = stats->second.txBytes;
                }
            }
          //   else
          //   {
          // 	  std::cout<<ns3::Ipv4Address(fiveTuple.sourceAddress)<<" == "<<it->second<<std::endl;
          //   }
        }
    }
  std::cout << "array de Vazao:";
  for (int i = 0; i < 19; i++)
    {
      std::cout << "\n" << rrhVazao[i] << " ";
    }
  std::cout << "\n" << std::endl;
  std::cout << "------------------------------------------" << std::endl;
  firstRun = false;
  Simulator::Schedule (Seconds (interval), &QoSMetricas, fmhelper, monitor, numberOfNodes, interval,
                       MyCtrl);
  MyCtrl->Allocation (mymap, mymap2, rrhVazao);
}
//-----------------------------------------------------Configurações
int
main (int argc, char *argv[])
{
  uint16_t numberOfRrhs = 19; //19
  uint16_t numberOfNodes = 650; //200
  uint16_t video_nodes = 0;

  uint16_t run = 1;
  uint16_t seed = 1;
  //bool PSO = false;

  double simTime = 180.0; //180.0
  double distance = 500.0; //500.0
  //double interPacketInterval = 50;
  bool trace = false;
  //ns3::SeedManager::SetSeed (seed);
  //ns3::SeedManager::SetRun (run);
  int appType = 1; //1 == UDP, 2 == WEB, 3 == Evalvid, 4 == OnOff
  double interval = 4.0; // Intervalo entre chamadas do flow monitor

  //LogComponentEnable ("Ipv4StaticRouting", LOG_LEVEL_ALL);

  // Command line arguments
  CommandLine cmd;
  cmd.AddValue ("simTime", "Total duration of the simulation [s])", simTime);
  cmd.AddValue ("distance", "Distance between eNBs [m]", distance);
  cmd.AddValue ("seed","seed", seed);
  cmd.AddValue ("run","run", run);
  //cmd.AddValue ("PSO","PSO", PSO);
  //cmd.AddValue("interPacketInterval", "Inter packet interval [ms])", interPacketInterval);
  cmd.Parse (argc, argv);

  ns3::SeedManager::SetSeed (seed);
  ns3::SeedManager::SetRun (run);
  Mac48Address::Allocate ();
  Ptr<LteHelper> lteHelper = CreateObject<LteHelper> ();
  Ptr<PointToPointEpcHelper> epcHelper = CreateObject<PointToPointEpcHelper> ();
  lteHelper->SetEpcHelper (epcHelper);
  epcHelper->SetAttribute ("S1uLinkMtu", UintegerValue (7000));
  epcHelper->SetAttribute ("S1uLinkDelay", TimeValue (Seconds (0.00015)));
  epcHelper->SetAttribute ("S1uLinkDataRate", DataRateValue (DataRate ("70Gb/s")));
  lteHelper->SetSchedulerType ("ns3::RrFfMacScheduler");
  //lteHelper->SetHandoverAlgorithmType ("ns3::NoOpHandoverAlgorithm"); // disable automatic handover

  lteHelper->SetHandoverAlgorithmType ("ns3::A2A4RsrqHandoverAlgorithm");
  lteHelper->SetHandoverAlgorithmAttribute ("ServingCellThreshold", UintegerValue (30));
  lteHelper->SetHandoverAlgorithmAttribute ("NeighbourCellOffset", UintegerValue (1));

  ConfigStore inputConfig;
  inputConfig.ConfigureDefaults ();

  GlobalValue::Bind ("ChecksumEnabled", BooleanValue (true));
  Config::SetDefault ("ns3::LteEnbRrc::SrsPeriodicity", UintegerValue (160));
  //Config::SetDefault ("ns3::OFSwitch13Device::PipelineCapacity", DataRateValue (DataRate ("1.5Mb/s")));
  lteHelper->SetAttribute ("PathlossModel", StringValue ("ns3::Cost231PropagationLossModel"));

  // parse again so you can override default values from the command line
  cmd.Parse (argc, argv);

  Ptr<Node> pgw = epcHelper->GetPgwNode ();
  //Mac48Address::Allocate ();
  // Create a single RemoteHost
  //-----------------------------------------------------Nós
  NodeContainer remoteHostContainer;
  NodeContainer mainswitchC;
  NodeContainer topswitchC;
  mainswitchC.Create (1);
  topswitchC.Create (1);
  Ptr<Node> topswitch = topswitchC.Get (0);
  Ptr<Node> mainswitch = mainswitchC.Get (0);
  NodeContainer midswitch;
  midswitch.Create (6);
  remoteHostContainer.Create (1);
  Ptr<Node> remoteHost = remoteHostContainer.Get (0);
  InternetStackHelper internet;
  internet.Install (remoteHostContainer);
  //-----------------------------------------------------Links
  CsmaHelper csmaHelper;
  csmaHelper.SetChannelAttribute ("DataRate", DataRateValue (DataRate ("1Gbps"))); //600Gbps
  csmaHelper.SetChannelAttribute ("Delay", TimeValue (NanoSeconds (15))); //10
  csmaHelper.SetDeviceAttribute ("Mtu", UintegerValue (9000));
  //csmaHelper.SetQueue ("ns3::DropTailQueue","MaxPackets", UintegerValue (65536));
  Config::SetDefault ("ns3::QueueBase::MaxSize", StringValue ("65536p"));
  NetDeviceContainer hostDevices;
  NetDeviceContainer switchPorts[8];
  NodeContainer pair (remoteHost, topswitch);
  NetDeviceContainer link = csmaHelper.Install (pair);
  hostDevices.Add (link.Get (0));
  switchPorts[0].Add (link.Get (1));
  pair = NodeContainer (topswitch, midswitch.Get (0));
  link = csmaHelper.Install (pair);
  switchPorts[0].Add (link.Get (0));
  switchPorts[1].Add (link.Get (1));
  pair = NodeContainer (midswitch.Get (0), mainswitch);
  link = csmaHelper.Install (pair);
  switchPorts[1].Add (link.Get (0));
  switchPorts[3].Add (link.Get (1));
  pair = NodeContainer (topswitch, midswitch.Get (1));
  link = csmaHelper.Install (pair);
  switchPorts[0].Add (link.Get (0));
  switchPorts[2].Add (link.Get (1));
  pair = NodeContainer (midswitch.Get (1), mainswitch);
  link = csmaHelper.Install (pair);
  switchPorts[2].Add (link.Get (0));
  switchPorts[3].Add (link.Get (1));

  pair = NodeContainer (topswitch, midswitch.Get (2));
  link = csmaHelper.Install (pair);
  switchPorts[0].Add (link.Get (0));
  switchPorts[4].Add (link.Get (1));
  pair = NodeContainer (midswitch.Get (2), mainswitch);
  link = csmaHelper.Install (pair);
  switchPorts[4].Add (link.Get (0));
  switchPorts[3].Add (link.Get (1));

  pair = NodeContainer (topswitch, midswitch.Get (3));
  link = csmaHelper.Install (pair);
  switchPorts[0].Add (link.Get (0));
  switchPorts[5].Add (link.Get (1));
  pair = NodeContainer (midswitch.Get (3), mainswitch);
  link = csmaHelper.Install (pair);
  switchPorts[5].Add (link.Get (0));
  switchPorts[3].Add (link.Get (1));

  pair = NodeContainer (topswitch, midswitch.Get (4));
  link = csmaHelper.Install (pair);
  switchPorts[0].Add (link.Get (0));
  switchPorts[6].Add (link.Get (1));
  pair = NodeContainer (midswitch.Get (4), mainswitch);
  link = csmaHelper.Install (pair);
  switchPorts[6].Add (link.Get (0));
  switchPorts[3].Add (link.Get (1));

  pair = NodeContainer (topswitch, midswitch.Get (5));
  link = csmaHelper.Install (pair);
  switchPorts[0].Add (link.Get (0));
  switchPorts[7].Add (link.Get (1));
  pair = NodeContainer (midswitch.Get (5), mainswitch);
  link = csmaHelper.Install (pair);
  switchPorts[7].Add (link.Get (0));
  switchPorts[3].Add (link.Get (1));

  pair = NodeContainer (pgw, mainswitch);
  link = csmaHelper.Install (pair);
  hostDevices.Add (link.Get (0));
  switchPorts[3].Add (link.Get (1));

  // Create the controller node
  Ptr<Node> controllerNode = CreateObject<Node> ();
  Ptr<Node> controllerNode2 = CreateObject<Node> ();

  //Ptr<OFSwitch13InternalHelper> of13Helper = CreateObject<OFSwitch13InternalHelper> ();
  //Ptr<OFSwitch13LearningController> learnCtrl = CreateObject<OFSwitch13LearningController> ();
  OFSwitch13DeviceContainer ofSwitchDevices;
  //of13Helper->InstallController (controllerNode, learnCtrl);

  // Configure the OpenFlow network domain
  Ptr<OFSwitch13InternalHelper> ofMyCtrlHelper = CreateObject<OFSwitch13InternalHelper> ();
  Ptr<MyController> MyCtrl = CreateObject<MyController> ();
  ofMyCtrlHelper->InstallController (controllerNode2, MyCtrl);

  ofSwitchDevices.Add (ofMyCtrlHelper->InstallSwitch (mainswitch, switchPorts[3]));
  ofSwitchDevices.Add (ofMyCtrlHelper->InstallSwitch (topswitch, switchPorts[0]));
  ofSwitchDevices.Add (ofMyCtrlHelper->InstallSwitch (midswitch.Get (0), switchPorts[1]));
  ofSwitchDevices.Add (ofMyCtrlHelper->InstallSwitch (midswitch.Get (1), switchPorts[2]));
  ofSwitchDevices.Add (ofMyCtrlHelper->InstallSwitch (midswitch.Get (2), switchPorts[4]));
  ofSwitchDevices.Add (ofMyCtrlHelper->InstallSwitch (midswitch.Get (3), switchPorts[5]));
  ofSwitchDevices.Add (ofMyCtrlHelper->InstallSwitch (midswitch.Get (4), switchPorts[6]));
  ofSwitchDevices.Add (ofMyCtrlHelper->InstallSwitch (midswitch.Get (5), switchPorts[7]));
  ofMyCtrlHelper->CreateOpenFlowChannels ();

  //ofSwitchDevices.Get(0).SetAttribute("ns3::OFSwitch13Device::PipelineCapacity", DataRateValue (DataRate ("1Mb/s")));
  //std::cout<<ofSwitchDevices.Get(0)->SetAttribute("&OFSwitch13Device::m_pipeCapacity",DataRateValue (DataRate ("1Mb/s")))<<std::endl;

  ofSwitchDevices.Get (2)->SetAttribute ("PipelineCapacity", DataRateValue (DataRate ("15Mb/s")));
  ofSwitchDevices.Get (3)->SetAttribute ("PipelineCapacity", DataRateValue (DataRate ("15Mb/s")));
  ofSwitchDevices.Get (4)->SetAttribute ("PipelineCapacity", DataRateValue (DataRate ("15Mb/s")));
  ofSwitchDevices.Get (5)->SetAttribute ("PipelineCapacity", DataRateValue (DataRate ("15Mb/s")));
  ofSwitchDevices.Get (6)->SetAttribute ("PipelineCapacity", DataRateValue (DataRate ("15Mb/s")));
  ofSwitchDevices.Get (7)->SetAttribute ("PipelineCapacity", DataRateValue (DataRate ("15Mb/s")));

  ofSwitchDevices.Get (0)->SetAttribute ("TcamDelay", TimeValue (MicroSeconds (0)));
  ofSwitchDevices.Get (1)->SetAttribute ("TcamDelay", TimeValue (MicroSeconds (0)));
  //of13Helper->CreateOpenFlowChannels ();

  Ipv4AddressHelper ipv4h;
  ipv4h.SetBase ("1.0.0.0", "255.255.255.0", "0.0.0.2");
  Ipv4InterfaceContainer internetIpIfaces = ipv4h.Assign (hostDevices);
  Ipv4Address remoteHostAddr = internetIpIfaces.GetAddress (0);
  Ipv4Address pgwAddr = internetIpIfaces.GetAddress (1);

  Ipv4StaticRoutingHelper ipv4RoutingHelper;
  Ptr<Ipv4StaticRouting> remoteHostStaticRouting =
      ipv4RoutingHelper.GetStaticRouting (remoteHost->GetObject<Ipv4> ());
  remoteHostStaticRouting->AddNetworkRouteTo (Ipv4Address ("7.0.0.0"), Ipv4Mask ("255.0.0.0"),
                                              pgwAddr, 1, 1);
  //remoteHostStaticRouting->SetDefaultRoute (internetIpIfaces.GetAddress(1), 1,1);

  NodeContainer enbNodes;
  enbNodes.Create (numberOfRrhs);
  //-----------------------------------------------------Mobilidade
  Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();
  for (uint16_t i = 0; i < numberOfRrhs; i++)
    {
      positionAlloc->Add (Vector (50, distance * i, 0));
    }
  MobilityHelper mobility;
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.SetPositionAllocator ("ns3::GridPositionAllocator", "MinX", DoubleValue (100.0), "MinY",
                                 DoubleValue (0.0), "DeltaX", DoubleValue (distance / 2), "DeltaY",
                                 DoubleValue (distance / 2), "GridWidth", UintegerValue (4),
                                 "LayoutType", StringValue ("RowFirst"));
  //mobility.SetPositionAllocator(positionAlloc);
  mobility.Install (enbNodes);
  Ptr<ListPositionAllocator> positionAlloc2 = CreateObject<ListPositionAllocator> ();
  positionAlloc2->Add (Vector (0, 10, 0)); // Server
  positionAlloc2->Add (Vector (10, 10, 0)); // top
  positionAlloc2->Add (Vector (25, 10, 0)); // mid1
  positionAlloc2->Add (Vector (25, 20, 0)); // mid2
  positionAlloc2->Add (Vector (25, 30, 0)); // mid3
  positionAlloc2->Add (Vector (25, 40, 0)); // mid4
  positionAlloc2->Add (Vector (25, 50, 0)); // mid5
  positionAlloc2->Add (Vector (25, 60, 0)); // mid6
  positionAlloc2->Add (Vector (40, 10, 0)); // main
  MobilityHelper mobility2;
  mobility2.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility2.SetPositionAllocator (positionAlloc2);
  mobility2.Install (NodeContainer (remoteHostContainer, topswitch, midswitch, mainswitch));
  Ptr<ListPositionAllocator> positionAlloc3 = CreateObject<ListPositionAllocator> ();
  positionAlloc3->Add (Vector (10, 20, 0)); // ctrl1
  positionAlloc3->Add (Vector (40, 20, 0)); // ctrl2
  positionAlloc3->Add (Vector (50, 10, 0)); // pgw
  MobilityHelper mobility3;
  mobility3.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility3.SetPositionAllocator (positionAlloc3);
  mobility3.Install (NodeContainer (controllerNode, controllerNode2, pgw));
  // Install LTE Devices to the nodes
  NetDeviceContainer enbLteDevs = lteHelper->InstallEnbDevice (enbNodes);

  //--------------------------------------------Nós moveis
  NodeContainer ueNodes;

  NodeContainer ueNodeswalk;
  NodeContainer ueNodescar;

  MobilityHelper uesMobilitywalk;
  MobilityHelper uesMobilitycar;

  ueNodeswalk.Create ((numberOfNodes * 7) / 10);
  uesMobilitywalk.SetPositionAllocator (
      "ns3::RandomBoxPositionAllocator", "X",
      StringValue ("ns3::UniformRandomVariable[Min=100.0|Max=1100.0]"), "Y",
      StringValue ("ns3::UniformRandomVariable[Min=0.0|Max=1000.0]"), "Z",
      StringValue ("ns3::UniformRandomVariable[Min=0|Max=0]"));
  uesMobilitywalk.SetMobilityModel ("ns3::RandomWalk2dMobilityModel", "Mode", StringValue ("Time"),
                                    "Time", StringValue ("1s"), "Speed",
                                    StringValue ("ns3::ConstantRandomVariable[Constant=2.25]"),
                                    "Bounds", RectangleValue (Rectangle (100, 1100, 0, 1000)));
  uesMobilitywalk.Install (ueNodeswalk);

  ueNodescar.Create ((numberOfNodes * 3) / 10);
  uesMobilitycar.SetPositionAllocator (
      "ns3::RandomBoxPositionAllocator", "X",
      StringValue ("ns3::UniformRandomVariable[Min=100.0|Max=1100.0]"), "Y",
      StringValue ("ns3::UniformRandomVariable[Min=0.0|Max=1000.0]"), "Z",
      StringValue ("ns3::UniformRandomVariable[Min=0|Max=0]"));
  uesMobilitycar.SetMobilityModel ("ns3::RandomWalk2dMobilityModel", "Mode", StringValue ("Time"),
                                   "Time", StringValue ("1s"), "Speed",
                                   StringValue ("ns3::ConstantRandomVariable[Constant=13.9]"),
                                   "Bounds", RectangleValue (Rectangle (100, 1100, 0, 1000)));
  uesMobilitycar.Install (ueNodescar);

  ueNodes.Add (ueNodeswalk);
  ueNodes.Add (ueNodescar);
  NetDeviceContainer ueLteDevs;

  ueLteDevs = lteHelper->InstallUeDevice (ueNodes);

  //--------------------------------------------Nós moveis (video)
  NodeContainer video_Nodes_Container;
  MobilityHelper video_uesMobility;
  //double yvalue=0;

  //yvalue= i*distance;
  video_Nodes_Container.Create (video_nodes);
  // uesMobility[i].SetPositionAllocator("ns3::RandomDiscPositionAllocator",
  //                                 "X", StringValue ("50.0"),
  //                                 //"Y", StringValue (std::to_string(yvalue)),
  //                                 "Y", DoubleValue (105.0),
  //                                 "Rho", StringValue ("ns3::UniformRandomVariable[Min=0|Max=30]"));
  video_uesMobility.SetPositionAllocator (
      "ns3::RandomBoxPositionAllocator", "X",
      StringValue ("ns3::UniformRandomVariable[Min=0.0|Max=375.0]"),
      //"Y", StringValue (std::to_string(yvalue)),
      "Y", StringValue ("ns3::UniformRandomVariable[Min=0.0|Max=125.0]"), "Z",
      StringValue ("ns3::UniformRandomVariable[Min=0|Max=0]"));
  video_uesMobility.SetMobilityModel ("ns3::RandomWalk2dMobilityModel", "Mode",
                                      StringValue ("Time"), "Time", StringValue ("1s"), "Speed",
                                      StringValue ("ns3::ConstantRandomVariable[Constant=2.25]"),
                                      "Bounds", RectangleValue (Rectangle (0, 375, 0, 125)));
  video_uesMobility.Install (video_Nodes_Container);

  NetDeviceContainer video_ueLteDevs;

  video_ueLteDevs = lteHelper->InstallUeDevice (video_Nodes_Container);
  //-----------------------------------------------------Routing
  internet.Install (ueNodes);

  Ipv4InterfaceContainer ueIpIface;

  ueIpIface = epcHelper->AssignUeIpv4Address (NetDeviceContainer (ueLteDevs));

  for (uint32_t u = 0; u < numberOfNodes; u++)
    {
      Ptr<Node> ueNode_sg = ueNodes.Get (u);
      Ptr<Ipv4StaticRouting> ueStaticRouting =
          ipv4RoutingHelper.GetStaticRouting (ueNode_sg->GetObject<Ipv4> ());
      ueStaticRouting->SetDefaultRoute (epcHelper->GetUeDefaultGatewayAddress (), 1);
    }
  // for (int i = 0; i < numberOfRrhs; i++)
  // {
  // for (uint16_t u = 0; u < numberOfNodes[i]; u++)
  // 	{
  // 		lteHelper->Attach (ueLteDevs[i].Get(u), enbLteDevs.Get(i));
  // 	}
  // }

  lteHelper->Attach (ueLteDevs);

  //-----------------------------------------------------Routing (evalvid)
  internet.Install (video_Nodes_Container);

  Ipv4InterfaceContainer video_ueIpIface;

  video_ueIpIface = epcHelper->AssignUeIpv4Address (NetDeviceContainer (video_ueLteDevs));

  for (uint32_t u = 0; u < video_nodes; u++)
    {
      Ptr<Node> ueNode_sg = video_Nodes_Container.Get (u);
      Ptr<Ipv4StaticRouting> video_ueStaticRouting =
          ipv4RoutingHelper.GetStaticRouting (ueNode_sg->GetObject<Ipv4> ());
      video_ueStaticRouting->SetDefaultRoute (epcHelper->GetUeDefaultGatewayAddress (), 1);
    }
  // for (int i = 0; i < numberOfRrhs; i++)
  // {
  // for (uint16_t u = 0; u < numberOfNodes[i]; u++)
  // 	{
  // 		lteHelper->Attach (ueLteDevs[i].Get(u), enbLteDevs.Get(i));
  // 	}
  // }

  lteHelper->Attach (video_ueLteDevs);
  //-----------------------------------------------------Bearer
  for (uint32_t u = 0; u < numberOfNodes; u++)
    {
      Ptr<NetDevice> ueDevice = ueLteDevs.Get (u);
      GbrQosInformation qos;
      qos.gbrDl = 132; // bit/s, considering IP, UDP, RLC, PDCP header size
      qos.gbrUl = 132;
      qos.mbrDl = qos.gbrDl;
      qos.mbrUl = qos.gbrUl;

      enum EpsBearer::Qci q = EpsBearer::GBR_CONV_VOICE;
      EpsBearer bearer (q, qos);
      bearer.arp.preemptionCapability = true;
      bearer.arp.preemptionVulnerability = true;
      lteHelper->ActivateDedicatedEpsBearer (ueDevice, bearer, EpcTft::Default ());
    }
  //-----------------------------------------------------Aplicação

  if (appType == 4)
    {
      //OnOff
      uint16_t ulPort = 900;

      Ptr<NormalRandomVariable> uv = CreateObject<NormalRandomVariable> ();
      uv->SetAttribute("Mean", DoubleValue(800));
      uv->SetAttribute("Variance", DoubleValue(50));
      uv->SetAttribute("Bound", DoubleValue(200));

      OnOffHelper onoff ("ns3::UdpSocketFactory",
                         Address (InetSocketAddress (remoteHostAddr, ulPort)));
      onoff.SetAttribute("DataRate", DataRateValue(uv->GetValue()));
      ApplicationContainer clientApps = onoff.Install (ueNodes);
      clientApps.Start (Seconds (2.0));
      clientApps.Stop (Seconds (simTime));
    }

  if (appType == 3)
    {
      //Evalvid
      //uint16_t Port = 9;
      for (uint32_t z = 0; z < video_Nodes_Container.GetN (); z++)
        {
          //string video_trans = "st_container_cif_h264_300_20.st";
          std::string video_trans = "st_highway_cif.st";
          std::string caminho = "./";
          std::stringstream sdTrace;
          std::stringstream rdTrace;
          sdTrace << caminho + "sd_a01_" << (int) z;
          rdTrace << caminho + "rd_a01_" << (int) z;
          //Port+=1;
          //   EvalvidServerHelper server (Port);
          //   server.SetAttribute ("SenderTraceFilename", StringValue(video_trans));
          //   server.SetAttribute ("SenderDumpFilename", StringValue(sdTrace.str()));
          //   server.SetAttribute ("PacketPayload",UintegerValue(1014));
          //   ApplicationContainer apps;
          //   apps.Add(server.Install(remoteHost));
          //   apps.Start (Seconds (2.0));
          //  apps.Stop (Seconds (simTime));

          //   EvalvidClientHelper client (remoteHostAddr,Port);
          //   client.SetAttribute ("ReceiverDumpFilename", StringValue(rdTrace.str()));
          //   apps.Add(client.Install (video_Nodes_Container.Get(z)));
          //   apps.Start (Seconds (2.0));
          //   apps.Stop (Seconds (simTime));
        }
    }
  if (appType == 2)
    {
      // Create HTTP server helper
      ThreeGppHttpServerHelper serverHelper (remoteHostAddr);

      // Install HTTP server
      ApplicationContainer serverApps = serverHelper.Install (remoteHostContainer.Get (0));
      Ptr<ThreeGppHttpServer> httpServer = serverApps.Get (0)->GetObject<ThreeGppHttpServer> ();
      // Setup HTTP variables for the server
      PointerValue varPtr;
      httpServer->GetAttribute ("Variables", varPtr);
      Ptr<ThreeGppHttpVariables> httpVariables = varPtr.Get<ThreeGppHttpVariables> ();
      httpVariables->SetMainObjectSizeMean (102400); // 100kB
      httpVariables->SetMainObjectSizeStdDev (40960); // 40kB

      // Create HTTP client helper
      ThreeGppHttpClientHelper clientHelper (remoteHostAddr);

      // Install HTTP client
      ApplicationContainer clientApps;
      for (uint32_t u = 0; u < numberOfNodes; u++)
        {
          clientApps.Add (clientHelper.Install (ueNodes.Get (u)));
          //Ptr<ThreeGppHttpClient> httpClient = clientApps.Get (u)->GetObject<ThreeGppHttpClient> ();
        }

      serverApps.Start (Seconds (2.0));
      serverApps.Stop (Seconds (simTime));
      clientApps.Start (Seconds (2.0));
      clientApps.Stop (Seconds (simTime));
    }
  if (appType == 1)
    {
      //uint16_t dlPort = 900;
      uint16_t ulPort = 900;

      UdpEchoServerHelper server (ulPort);
      ApplicationContainer serverApps = server.Install (remoteHost);

      ApplicationContainer clientApps[numberOfNodes];
      UdpEchoClientHelper udpUlClient (remoteHostAddr, ulPort);
      udpUlClient.SetAttribute ("Interval", TimeValue (MilliSeconds (40)));
      udpUlClient.SetAttribute ("MaxPackets", UintegerValue (1000000));
      udpUlClient.SetAttribute ("PacketSize", UintegerValue (2048));

      for (uint32_t u = 0; u < numberOfNodes; u++)
        {
          Ptr<UniformRandomVariable> startTimeSeconds = CreateObject<UniformRandomVariable> ();
          srand (u);
          startTimeSeconds->SetAttribute ("Min", DoubleValue (2.0));
          startTimeSeconds->SetAttribute ("Max", DoubleValue (5.0));
          //clientApps.Add (dlClient.Install (remoteHost));
          clientApps[u].Add (udpUlClient.Install (ueNodes.Get (u)));
          clientApps[u].Start (Seconds (startTimeSeconds->GetValue ()));
          //clientApps[u].Start (Seconds (2.0));
          clientApps[u].Stop (Seconds (simTime));
          //std::cout<<startTimeSeconds->GetValue()<<std::endl;
        }
      serverApps.Start (Seconds (2.0));
      serverApps.Stop (Seconds (simTime));
    }

  Ptr<FlowMonitor> flowmon;
  FlowMonitorHelper flowmonHelper;
  flowmon = flowmonHelper.Install (ueNodes);
  flowmon = flowmonHelper.Install (remoteHostContainer);
  ofMyCtrlHelper->EnableDatapathStats ("switch-stats");
  if (trace)
    {
      ofMyCtrlHelper->EnableOpenFlowPcap ("openflow");
      csmaHelper.EnablePcap ("mainswitch", mainswitchC);
      csmaHelper.EnablePcap ("topswitch", topswitchC);
      csmaHelper.EnablePcap ("midswitch", midswitch);
      csmaHelper.EnablePcap ("server", hostDevices);
      //lteHelper->EnableTraces ();
      lteHelper->EnablePhyTraces ();
      lteHelper->EnableMacTraces ();
      lteHelper->EnableRlcTraces ();
      lteHelper->EnablePdcpTraces ();
    }

  //AnimationInterface anim ("animation.xml");
  Simulator::Stop (Seconds (simTime));
  //anim.SetMaxPktsPerTraceFile (999999);
  //-----------------------------------------------------Handover
  lteHelper->AddX2Interface (enbNodes);

  Config::Connect ("/NodeList/*/DeviceList/*/LteUeRrc/ConnectionEstablished",
                   MakeCallback (&NotifyConnectionEstablishedUe));
  Config::Connect ("/NodeList/*/DeviceList/*/LteEnbRrc/HandoverEndOk",
                   MakeCallback (&MyController::Update, MyCtrl));
  //-----------------------------------------------------Schedule/Callbacks
  Simulator::Schedule (Seconds (0.5), &CallMap, epcHelper);
  Simulator::Schedule (Seconds (1.5), &SendToController, MyCtrl, rrhVazao);
  //Simulator::Schedule (Seconds (15.0), &SendToController, MyCtrl);
  Simulator::Schedule (Seconds (6.0), &QoSMetricas, &flowmonHelper, flowmon, numberOfNodes,
                       interval, MyCtrl);

  Simulator::Run ();
  Simulator::Destroy ();
  flowmon->SerializeToXmlFile ("switch.flowmon", false, false);

  return 0;
}
