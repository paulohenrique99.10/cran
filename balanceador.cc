#include "balanceador.h"
#include <cstring>
#include <iostream>
#include <cmath>
#include <algorithm>
#include <ctime>
#include <vector>
#include <numeric>
#include <string>
#include <functional>

#define rand_01 ((int) rand () / (int) RAND_MAX) //numero randomico (?)

using namespace std;

int *
retornaVetor (int n, double rrhVazao[19])
{
  if (n == 2)
    {
      static int vet[19] = {3, 3, 2, 5, 5, 2, 1, 5, 5, 6, 6, 1, 4, 4, 6, 6, 4, 4, 6};
      return vet;
    }

  //PSO(vazao)
  //return *gbest
  time_t t;
  srand ((unsigned) time (&t)); // Inicia semente randomica

  int xmin[numofdims], xmax[numofdims]; // vetores com valores maximos e minimos para a populacao
  int initpop[50][numofdims]; // matriz com a populacao inicial
  // int worsts[1000], bests[1000]; // melhores e piores fitness
  // int meanfits[1000]; // vetor com o fitness medio das iteracoes
  int gbestfit; // melhor fitness global
  int gbest[numofdims]; // melhor candidato
  for (int i = 0; i < numofdims; i++)
    {
      xmax[i] = 5;
      xmin[i] = 0;
    }
  for (int i = 0; i < 50; i++)
    for (int j = 0; j < numofdims; j++)
      {
        initpop[i][j] =
            1 + (rand () %
                 static_cast<int> (
                     6 - 1 + 1)); // cria a populacao inicial com valores randomicos entre 1 e 6
        //rand() % (6 + 6 + 1) - 100
        //cout << "valor aleatorio: " << initpop[i][j] << endl;
      }

  PSO (100, 1.8, 1.8, xmin, xmax, initpop, &gbestfit, gbest, rrhVazao); // worsts, meanfits, bests,

  cout << "fitness: " << gbestfit << "\nmelhor candidato:" << endl;
  for (int i = 0; i < numofdims; i++)
    {
      std::cout << gbest[i] << ' ';
    }
  int *retorno = gbest;
  return retorno;
}

void fitnessfunc (
    int X[numofparticles][numofdims], int fitnesses[numofparticles],
    double rrhVazao
        [numofdims]) //argumentos sao uma matriz X com a copia da populacao e fitnesses, um vetor com os fitness de cada elemento (?)
{

  // cout << "rrhVazao:" << endl;
  // for (int i = 0; i < numofdims; i++)
  //   {
  //     std::cout << rrhVazao[i] << ' ';
  //   }
  memset (fitnesses, 0, sizeof (int) * numofparticles); //apaga o vetor
  double bbuVazao[6];
  for (int i = 0; i < numofparticles; i++)
    {
      for (int f = 0; f < 6; f++)
        {
          bbuVazao[f] = 0;
        }
      for (int j = 0; j < numofdims; j++)
        {
          bbuVazao[(X[i][j]) - 1] += rrhVazao
              [j]; // array bbuVazao recebe, na posicao referente à bbu do candidato, a vazao do rhVazao
          //cout<<"rrhVazao: "<<j<<" <-"<<rrhVazao[j]<<endl;
          //cout << "bbuVazao: " << bbuVazao[X[i][j] - 1] << endl;
        }
      for (int z = 0; z < 6; z++)
        {
          //cout<<(bbuVazao[z]-15000.0)*(bbuVazao[z]-15000.0)<<endl;
          fitnesses[i] += static_cast<int> ((bbuVazao[z] - 15000.0) * (bbuVazao[z] - 15000.0));
        }
      //cout<<"Fitness de i= "<<i<<" :"<<fitnesses[i]<<endl;
    }
  // for (int i = 0; i <6; i++)
  // {
  //   std::cout<<"Vazao "<<bbuVazao[i] << ' ';
  // }
}

//FUNCAO PARA RETORNO DE MEDIA DE UM VETOR
int
mean (int inputval[], int vallength)
{
  int addvalue = 0;
  for (int i = 0; i < vallength; i++)
    {
      addvalue += inputval[i];
    }
  return (int) (addvalue / vallength);
}

void PSO (int numofiterations, float c1, float c2, int Xmin[numofdims], int Xmax[numofdims],
          int initialpop[numofparticles][numofdims], int *gbestfit, int gbest[numofdims],
          double rrhVazao[numofdims]) //int worsts[], int meanfits[], int bests[],
{
  int V[numofparticles][numofdims] = {0}; // Matriz de velocidades, nula
  int X[numofparticles][numofdims]; // Matriz que recebe a populacao inicial
  int Vmax[numofdims]; // valor maximo dos candidatos
  int Vmin[numofdims]; // valor minimo dos candidatos
  int pbests[numofparticles][numofdims]; // matriz com os melhores candidatos
  int pbestfits[numofparticles]; // vetor com os fitness dos melhores candidatos
  int fitnesses[numofparticles]; // vetor com os valores de fitness
  //int w; // taxa de aprendizado
  int minfit; // ??
  int minfitidx; // ??

  memcpy (X, initialpop,
          sizeof (int) * numofparticles * numofdims); // copia a matriz da populacao inicial para X
  fitnessfunc (X, fitnesses,
               rrhVazao); // passa a populacao e o vetor de fitness para a funcao fitness
  minfit = *min_element (fitnesses, fitnesses + numofparticles); // recebe o melhor candidato
  minfitidx =
      min_element (fitnesses, fitnesses + numofparticles) - fitnesses; // recebe o melhor fitness
  *gbestfit = minfit; // melhor fitness
  memcpy (gbest, X[minfitidx],
          sizeof (int) *
              numofdims); //sobreescreve o gbest com a melhor particula da populacao inicial

  for (int i = 0; i < numofdims; i++)
    {
      Vmax[i] = 0.2 * (Xmax[i] - Xmin[i]); // 40
      Vmin[i] = -Vmax[i]; //-40
    }

  for (int t = 0; t < numofiterations; t++)
    {
      //w = 0.9 - 0.7 * t / numofiterations; // taxa de aprendizado

      for (int i = 0; i < numofparticles; i++)
        {
          if (fitnesses[i] < pbestfits[i])
            {
              pbestfits[i] = fitnesses
                  [i]; // se o fitness da geracao atual for o melhor da particula, ele substitui no vetor pbestfits
              memcpy (pbests[i], X[i],
                      sizeof (int) * numofdims); // sobreescreve o candidato na matriz de pbests
            }
        }
      for (int i = 0; i < numofparticles; i++)
        {
          for (int j = 0; j < numofdims; j++)
            {
              V[i][j] =
                  static_cast<int> (min (max (static_cast<int>((V[i][j] + rand_01 * c1 * (pbests[i][j] - X[i][j]) +
                                               rand_01 * c2 * (gbest[j] - X[i][j]))),
                                              Vmin[j]),
                                         Vmax[j]));
              /*
                V[i][j] = min(max((w * V[i][j] + rand_01 * c1 * (pbests[i][j] - X[i][j])
                                   + rand_01 * c2 * (gbest[j] - X[i][j])), Vmin[j]), Vmax[j]); // calculo da velocidade atual
                */
              X[i][j] = min (max ((X[i][j] + V[i][j]), Xmin[j]), Xmax[j]); // calculo do valor atual
            }
        }

      fitnessfunc (X, fitnesses, rrhVazao); // calcula o fitness da nova populacao
      minfit = *min_element (fitnesses, fitnesses + numofparticles); // recebe o melhor candidato
      minfitidx = min_element (fitnesses, fitnesses + numofparticles) -
                  fitnesses; // recebe o melhor fitness
      if (minfit < *gbestfit)
        {
          *gbestfit = minfit; // atualiza o valor do gbest
          memcpy (gbest, X[minfitidx],
                  sizeof (int) * numofdims); // sobreescreve o gbest antigo pelo novo
        }

      // worsts[t] = *max_element(fitnesses, fitnesses + numofparticles);//adiciona o pior fitness da rodada ao vetor
      // bests[t] = *gbestfit;//adiciona o melhor fitness da rodada ao vetor
      // meanfits[t] = mean(fitnesses, numofparticles);//adiciona a media do fitness da rodada ao vetor
    }
}