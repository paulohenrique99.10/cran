#ifndef BALANCEADOR_H 
#define BALANCEADOR_H

const int numofdims = 19; //tamanho da solucao  --- total de RRHs
const int numofparticles = 30; //numero de particulas

int * retornaVetor(int n, double rrhVazao[19]);
void PSO (int numofiterations, float c1, float c2,
              int Xmin[numofdims], int Xmax[numofdims], int initialpop[numofparticles][numofdims],
               int *gbestfit, int gbest[numofdims], double rrhVazao[19]);
#endif